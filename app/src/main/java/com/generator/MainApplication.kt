package com.generator

import android.app.Application
import androidx.room.Room
import com.generator.db.AppDatabase
import com.generator.di.AppModule
import com.generator.di.ApplicationComponent
import com.generator.di.DaggerApplicationComponent

class MainApplication : Application() {

    companion object {
        lateinit var appComponent: ApplicationComponent
    }

    lateinit var database: AppDatabase

    override fun onCreate() {
        super.onCreate()
        initAppComponent()
        initDB()
    }

    private fun initAppComponent() {
        appComponent = DaggerApplicationComponent.builder()
            .appModule(AppModule(this))
            .build()
    }

    private fun initDB() {
        database = Room.databaseBuilder(this, AppDatabase::class.java, "database")
            .allowMainThreadQueries()
            .fallbackToDestructiveMigration()
            .build()
    }
}