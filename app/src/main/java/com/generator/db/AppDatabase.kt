package com.generator.db

import androidx.room.Database
import androidx.room.RoomDatabase
import com.generator.model.NumberList

@Database(entities = [NumberList::class], version = 2, exportSchema = false)
abstract class AppDatabase : RoomDatabase() {

    abstract fun numberListDao(): NumberListDao
}