package com.generator.db

import androidx.room.Dao
import androidx.room.Insert
import androidx.room.Query
import com.generator.model.NumberList

@Dao
interface NumberListDao {

    @Query("SELECT * FROM numberList")
    fun getList(): NumberList?

    @Insert
    fun insert(numberList: NumberList)

    @Query("DELETE FROM numberList")
    fun clearTable()
}