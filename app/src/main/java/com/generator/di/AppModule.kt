package com.generator.di

import android.content.Context
import com.generator.MainApplication
import com.generator.db.AppDatabase
import com.generator.db.NumberListDao
import dagger.Module
import dagger.Provides
import javax.inject.Singleton

@Module
class AppModule(private val app: MainApplication) {

    @Provides
    @Singleton
    fun provideApplication(): MainApplication = app

    @Provides
    @Singleton
    fun provideApplicationContext(app: MainApplication): Context = app

    @Provides
    @Singleton
    fun provideNumberDao(app: MainApplication): NumberListDao = app.database.numberListDao()
}