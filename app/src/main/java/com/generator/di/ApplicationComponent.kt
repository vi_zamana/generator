package com.generator.di

import com.generator.ui.GeneratorFragment
import dagger.Component
import javax.inject.Singleton

@Component(modules = [AppModule::class])
@Singleton
interface ApplicationComponent {

    fun inject(fragment: GeneratorFragment)
}