package com.generator.di

import androidx.room.TypeConverter
import com.google.gson.Gson
import com.google.gson.reflect.TypeToken

object Converter {

    @TypeConverter
    @JvmStatic
    fun stringToIntList(json: String): List<Int> {
        val type = object : TypeToken<List<Int>>() {
        }.type
        return Gson().fromJson(json, type)
    }

    @TypeConverter
    @JvmStatic
    fun intListToString(list: List<Int>): String {
        val type = object : TypeToken<List<Int>>() {
        }.type
        return Gson().toJson(list, type)
    }
}
