package com.generator.model

import androidx.room.Entity
import androidx.room.PrimaryKey
import androidx.room.TypeConverters
import com.generator.di.Converter

@Entity
@TypeConverters(Converter::class)
data class NumberList(
    @PrimaryKey(autoGenerate = true)
    var id: Int = 0,
    var limitList: Int = 0,
    var list: List<Int> = emptyList()
)