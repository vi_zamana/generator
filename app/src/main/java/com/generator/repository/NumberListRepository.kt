package com.generator.repository

import com.generator.db.NumberListDao
import com.generator.model.NumberList
import javax.inject.Inject

class NumberListRepository @Inject constructor(private val dao: NumberListDao) {

    fun getList() =
        dao.getList()

    fun clearNumberLists() {
        dao.clearTable()
    }

    fun addList(list: NumberList) {
        clearNumberLists()
        dao.insert(list)
    }
}