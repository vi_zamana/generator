package com.generator.repository

import io.reactivex.Observable
import io.reactivex.Single
import io.reactivex.subjects.ReplaySubject
import javax.inject.Inject
import kotlin.random.Random

class NumberRepository @Inject constructor(private val numberListRepository: NumberListRepository) {

    private val subject = ReplaySubject.create<List<Int>>()

    fun observable(): Observable<List<Int>> = subject

    fun generateNumber(limit: Int) {
        Single.create<List<Int>> {
            it.onSuccess(getList(limit))
        }.subscribe { result ->
            subject.onNext(result)
        }
    }

    private fun getList(limit: Int): List<Int> {
        var start = 0
        val list = arrayListOf<Int>()
        val numberList = numberListRepository.getList()
        if (numberList != null) {
            if (limit > numberList.limitList) {
                start = numberList.limitList
                list.addAll(numberList.list)
                list.addAll(List(Random.nextInt(limit)) { Random.nextInt(start, limit) })
            } else {
                list.addAll(numberList.list.filter {
                    it < limit
                })
            }
        } else {
            list.addAll(List(Random.nextInt(start, limit)) { Random.nextInt(start, limit) })
        }
        return list.sorted().distinct()
    }
}