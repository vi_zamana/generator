package com.generator.ui

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import androidx.recyclerview.widget.LinearLayoutManager
import com.generator.MainApplication
import com.generator.R
import com.generator.databinding.FragmentGeneratorBinding
import com.generator.ui.adapter.NumberAdapter
import com.google.android.flexbox.FlexDirection
import com.google.android.flexbox.FlexboxLayoutManager
import com.google.android.flexbox.JustifyContent
import kotlinx.android.synthetic.main.fragment_generator.*
import javax.inject.Inject

class GeneratorFragment : Fragment() {

    @Inject
    lateinit var viewModelFactory: GeneratorViewModelFactory

    private lateinit var viewModel: GeneratorViewModel

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ) =
        DataBindingUtil.inflate<FragmentGeneratorBinding>(
            inflater, R.layout.fragment_generator, container, false
        ).run {
            lifecycleOwner = this@GeneratorFragment
            viewModel = this@GeneratorFragment.viewModel
            root
        }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        MainApplication.appComponent.inject(this)
        viewModel = ViewModelProviders.of(this, viewModelFactory)
            .get(GeneratorViewModel::class.java)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        viewModel.listNumber.observe(this, Observer {
            initList(it)
        })
    }

    private fun initList(list: List<Int>) {
        recyclerView.run {
            adapter = NumberAdapter({
                viewModel.calculateTime(it)
            }, list.count())
            layoutManager = FlexboxLayoutManager(context).apply {
                flexDirection = FlexDirection.ROW
                justifyContent = JustifyContent.SPACE_AROUND
            }
            (adapter as NumberAdapter).setItems(list)
        }
    }
}