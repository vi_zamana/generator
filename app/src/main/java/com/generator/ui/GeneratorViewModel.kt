package com.generator.ui

import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.generator.model.NumberList
import com.generator.repository.NumberListRepository
import com.generator.repository.NumberRepository
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.schedulers.Schedulers
import java.util.*

class GeneratorViewModel(
    private val dbRepository: NumberListRepository,
    private val numberRepository: NumberRepository
) : ViewModel() {

    val limit = MutableLiveData<String>()

    val listNumber = MutableLiveData<List<Int>>()

    val time = MutableLiveData<String>()

    var generateList = NumberList()

    private val disposables = CompositeDisposable()

    private var dateStart: Date? = null

    init {
        numberRepository.observable().subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe { result ->
                generateList = NumberList(
                    limitList = limit.value!!.toInt(),
                    list = result
                )
                listNumber.value = generateList.list
            }.let(disposables::add)
    }

    override fun onCleared() {
        super.onCleared()
        disposables.clear()
    }

    fun calculateTime(dateEnd: Date) {
        dateStart?.let {
            time.value = (dateEnd.time - it.time).toString()
        }
    }

    fun generate() {
        dateStart = Date()
        limit.value?.let {
            numberRepository.generateNumber(it.toInt())
        }
    }

    fun saveList() {
        dbRepository.addList(generateList)
    }

    fun clearLists() {
        dbRepository.clearNumberLists()
    }
}