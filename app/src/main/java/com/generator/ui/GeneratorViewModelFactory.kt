package com.generator.ui

import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import com.generator.repository.NumberListRepository
import com.generator.repository.NumberRepository
import javax.inject.Inject

@Suppress("UNCHECKED_CAST")
class GeneratorViewModelFactory @Inject constructor(private val dbRepository: NumberListRepository,
                                                    private val numberRepository: NumberRepository
) : ViewModelProvider.Factory {

    override fun <T : ViewModel> create(modelClass: Class<T>): T {
        return if (modelClass.isAssignableFrom(GeneratorViewModel::class.java)) {
            GeneratorViewModel(dbRepository, numberRepository) as T
        } else {
            throw IllegalArgumentException("ViewModel Not Found")
        }
    }

}