package com.generator.ui.adapter

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.recyclerview.widget.RecyclerView
import com.generator.R
import com.generator.databinding.ItemNumberBinding
import java.util.*

class NumberAdapter(
    private val onReady: (date: Date) -> Unit,
    private val count: Int
) :
    RecyclerView.Adapter<NumberViewHolder>() {

    private val itemList: ArrayList<Int> = arrayListOf()

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): NumberViewHolder {
        val inflater = LayoutInflater.from(parent.context)
        val itemBinding = DataBindingUtil.inflate(
            inflater, R.layout.item_number,
            parent, false
        ) as ItemNumberBinding
        return NumberViewHolder(itemBinding)
    }

    override fun getItemCount() = itemList.size

    override fun onBindViewHolder(holder: NumberViewHolder, position: Int) {
        holder.bind(itemList[position])
        if (position == count - 1) {
            onReady(Date())
        }
    }

    fun setItems(list: List<Int>) {
        itemList.addAll(list)
        notifyDataSetChanged()
    }
}