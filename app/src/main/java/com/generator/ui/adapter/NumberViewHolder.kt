package com.generator.ui.adapter

import androidx.recyclerview.widget.RecyclerView
import com.generator.databinding.ItemNumberBinding

class NumberViewHolder (private val itemBinding: ItemNumberBinding)
    : RecyclerView.ViewHolder(itemBinding.root) {

    fun bind(number: Int) {
        itemBinding.item = number
    }
}